This repository has a copy of my most up to date resume and a generic cover letter meant to give a quick introduction.

[Michael Berger Resume](https://gitlab.com/Stratoslogic/resume/blob/master/Resume.md)

[Cover Letter](https://gitlab.com/Stratoslogic/resume/blob/master/CoverLetterMB.md)